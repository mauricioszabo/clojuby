(ns clojuby.core-test
  (:require [clojure.test :refer [deftest testing is]]
            [check.core :refer [check]]
            [clojuby.core :as rb]))

(deftest convertion-from-ruby
  (testing "converts numbers"
    (check (rb/eval "1") => 1)
    (check (rb/eval "1.1") => 1.1))

  (testing "converts strings and symbols"
    (check (rb/eval "\"foo\"") => "foo")
    (check (rb/eval ":foo") => :foo))

  (testing "converts booleans"
    (check (rb/eval "true") => true)
    (check (rb/eval "false") => false)
    (check (rb/eval "nil") => nil))

  (testing "converts colections"
    (check (rb/eval "{a: 10}") => {:a 10})
    (check (rb/eval "[:a]") => [:a])
    (check (rb/eval "[:a, :b]") => [:a :b])
    (check (rb/eval "[1, 2, :a]") => [1 2 :a])
    (rb/rb-require "set")
    (check (rb/eval "Set[:a, :b]") => #{:a :b}))

  (testing "converts blocks"
    (let [f1 (rb/eval "proc { |x| x + 2}")
          f2 (rb/eval "proc { |x, &b| b.call(x) }")]
      (check (f1 10) => 12)
      (check (f2 10 (rb/& inc)) => 11)))

  (testing "binds self correctly on blocks"
    (check (rb/send [1 2 3] "instance_eval" (rb/&& (fn [self _] self)))
           => [1 2 3])))

(deftest conversion-to-ruby
  (testing "converts numbers"
    (check (rb/clj->rb 1) => (rb/raw-eval "1"))
    (check (rb/clj->rb 1.1) => (rb/raw-eval "1.1")))

  (testing "converts strings and symbols"
    (check (rb/clj->rb "foo") => (rb/raw-eval "\"foo\""))
    (check (rb/clj->rb :foo) => (rb/raw-eval ":foo")))

  (testing "converts booleans"
    (check (rb/clj->rb true) => (rb/raw-eval "true"))
    (check (rb/clj->rb false) => (rb/raw-eval "false"))
    (check (rb/clj->rb nil) => (rb/raw-eval "nil")))

  (testing "converts colections"
    (check (rb/clj->rb {:a 10}) => (rb/raw-eval "{a: 10}"))
    (check (rb/clj->rb [1 2 :a]) => (rb/raw-eval "[1, 2, :a]"))
    (check (rb/clj->rb #{1 2 :a}) => (rb/raw-eval "Set[1, 2, :a]")))

  (testing "converts functions"
    (let [plus-2 (rb/clj->rb (fn [a b] (+ a b)))
          plus-n (rb/clj->rb +)
          block-2 (rb/eval "proc { |x| x.call(1, 2) }")
          block-n (rb/eval "proc { |x| x.call(1, 2, 3, 4) }")
          block-block (rb/eval "proc { |x, &b| b.call(x, 1) }")]
      (check (block-2 plus-2) => 3)
      (check (block-n plus-n) => 10)
      (check (block-block 10 (rb/& +)) => 11))))

(deftest ruby-interpretation
  (testing "calls methods"
    (check (rb/send 10 "to_s") => "10")
    (check (rb/send 10 "to_s" 16) => "a"))

  (testing "calls methods with blocks"
    (check (rb/send (rb/eval "1..5") "map" (rb/& inc)) => [2 3 4 5 6])
    (check (rb/send& (rb/eval "1..5") "map" inc) => [2 3 4 5 6])
    (check (rb/send [1 2 3 4] "map" (rb/& inc)) => [2 3 4 5]))

  (testing "regexp calls"
    (check (rb/send "handmaid" "split" #"[nm]") => ["ha" "d" "aid"]))

  (testing "about class creation"
    (testing "creates simple class"
      (let [class (doto (rb/create-class! "Simple" rb/ruby-object)
                        (rb/add-method! "sum_two" (fn [_ a b] (+ a b))))
            instance (rb/new class)]
        (check (rb/send instance "sum_two" 10 20) => 30)))

    (testing "inherits class methods"
      (let [class (rb/create-class! "Inherit" (rb/raw-eval "File"))]
        (check (rb/send class "exist?" "foobar.baz") => false)))

    (testing "calls methods refering to self"
      (let [class (doto (rb/create-class! "Selfish" (rb/raw-eval "String"))
                        (rb/add-method! "append" (fn [{:keys [self]} a] (str self "-" a))))
            instance (rb/raw-new class (rb/clj->rb "some-str"))]
        (check (rb/send instance "append" "foo") => "some-str-foo")))

    (testing "creates class methods"
      (let [class (doto (rb/create-class! "Meth" rb/ruby-object)
                        (rb/add-method! "self.foo" (fn [_] "FOO")))]
        (check (rb/send class "foo") => "FOO")))

    (testing "refers to 'super'"
      (let [class (doto (rb/create-class! "Super" (rb/raw-eval "String"))
                        (rb/add-method! "upcase" (fn [{:keys [super self]}]
                                                   (str "-" (super) "-" self))))
            instance (rb/raw-new class (rb/clj->rb "str"))]
        (check (rb/send instance "upcase") => "-STR-str")))

    (testing "defines a constructor and accesses instance variables"
      (let [class (doto (rb/create-class! "Inst" (rb/raw-eval "String"))
                        (rb/add-method! "initialize" (fn [self var]
                                                       (rb/set-variable (:self self)
                                                                        "@var" var)))
                        (rb/add-method! "foo" (fn [self]
                                                (rb/get-variable (:self self) "@var"))))
            instance (rb/raw-new class (rb/clj->rb :some-var))]
        (check (rb/send instance "foo") => :some-var)))))

(def AStruct (rb/raw-eval "
Class.new {
  attr_accessor :a
  def initialize(a); @a = a; end
}
"))

(deftest conversion-of-non-edn
  (rb/raw-eval "require('ostruct')")
  (testing "will pretty-print the same as inspect"
    (check (pr-str (rb/eval "OpenStruct.new(a: 10)"))
           => (rb/eval "OpenStruct.new(a: 10).inspect")))

  (testing "will be able to get values as map"
    (let [struct (rb/new AStruct 10)]
      (check (:a struct) => 10)))

  (testing "responds-to?"
    (check (contains? (rb/eval "Object") :to-s) => true)
    (check (contains? (rb/eval "Object") :to-hash-code) => false))

  (testing "returns a new object with `assoc`..."
    (let [struct (rb/new AStruct (rb/new AStruct 10))
          new-struct (assoc-in struct [:a :a] "Hello")]
      (check (-> struct :a :a) => 10)
      (check (-> new-struct :a :a) => "Hello")))

  (testing "...but can also mutate the object"
    (let [struct (rb/new AStruct (rb/new AStruct 10))
          new-struct (assoc-in struct [:a= :a=] "Hello")]
      (check (-> struct :a :a) => "Hello")
      (check (-> new-struct :a :a) => "Hello"))))

(def ParamsLike (rb/raw-eval "
Class.new {
  def initialize(a); @hash = a; end
  def [](k); @hash[k]; end
  def []=(k, v); @hash[k] = v; end
  def dup; self.class.new(@hash.dup) end
}
"))

(deftest conversion-of-array-and-map-like
  (testing "will pretty-print the same as inspect"
    (check (pr-str (rb/new ParamsLike {:a 10 :b 20}))
           => #"@hash=\{:a"))

  (testing "will be able to get values as map"
    (let [struct (rb/new ParamsLike {:a 10})]
      (check (:a struct) => 10)))

  ; (testing "responds-to?"
  ;   (check (contains? (rb/eval "Object") :to-s) => true)
  ;   (check (contains? (rb/eval "Object") :to-hash-code) => false))

  (testing "returns a new object with `assoc`..."
    (let [struct (rb/new ParamsLike {:a (rb/new ParamsLike {:b 10})})
          new-struct (assoc-in struct [:a :b] "Hello")]
      (check (-> struct :a :b) => 10)
      (check (-> new-struct :a :b) => "Hello")))

  (testing "...but can also mutate the object"
    (let [struct (rb/new ParamsLike {:a (rb/new ParamsLike {:b 10})})
          new-struct (assoc-in struct [:a :b] "Hello")]
      (check (-> struct :a :b) => 10)
      (check (-> new-struct :a :b) => "Hello"))))

(deftest class-creation
  (testing "generates a Ruby class but don't add to Ruby"
    (rb/defclass SomeClass < rb/Array
      (initialize [{:keys [super self]} val] (super 2 val))
      (to-s [{:keys [super self]}] (str (super) "-" (first self))))
    (check (rb/send (rb/raw-new SomeClass (rb/clj->rb 5)) "to_s")
           => "[5, 5]-5"))

  (testing "extending and including modules on a class"
    (rb/defclass Pairs
      (extend rb/Enumerable)
      (include rb/Enumerable)
      (each [_ block] (block 1) (block 2))
      (self.each [_ block] (block 3) (block 4)))
    (check (rb/send (rb/new Pairs) "map" (rb/& inc)) => [2 3])
    (check (rb/send Pairs "map" (rb/& inc)) => [4 5]))

  (testing "allows to add/replace methods of a class"
    (let [new-class (rb/create-class! "SuperArray" (rb/raw-eval "Array"))]
      (rb/add-method! new-class "to_s" (fn [{:keys [super]}]
                                         (str (super) " yeah!")))
      (check (rb/send (rb/raw-new new-class (rb/clj->rb 2) (rb/clj->rb 2)) "to_s")
             => "[2, 2] yeah!"))))

(deftest sugared-syntax
  (testing "renames rb/* to pure Ruby calls"
    (check (rb/ruby rb/Object) => (rb/eval "Object")))

  (testing "calls methods on objects"
    (check (rb/ruby (. "foo" upcase)) => "FOO")
    (check (rb/ruby (. rb/Class to-s)) => "Class")
    (check (rb/ruby (. rb/File.Constants name)) => "File::Constants"))

  (testing "self test"
    (check (rb/ruby (. [1 2 3] instance-eval (&& (fn [self] self))))
           => [1 2 3])
    (check (rb/ruby (. rb/Object instance-eval (& (fn [self] self))))
           => (rb/raw-eval "Object"))))
