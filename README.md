# Clojuby

Because you can rubyfy Clojure, but cannot clojurify Ruby...

... or whatever that means

## Usage

(OUTDATED - Check tests for now)

## Hacking inside Rails

TBD

## Differences from JRuby

Clojuby is **less dynamic** than JRuby. This means that while you can monkey-patch existing classes, some methods will keep old code running. For example:

### Defining classes and methods

`super` is statically bound to the first ancestor that have the method. It means that `include`ing new modules will not make `super` point to other parent. For example, this code in Ruby runs like this:

```ruby
class SomeClass
  def to_s
    "Changed: #{super}"
  end
end
SomeClass.new.to_s
# => "Changed: #<SomeClass:0x000056169dc14198>"

module Stringify
  def to_s
    "Instance of #{self.class.to_s}"
  end
end

SomeClass.include Stringify
SomeClass.new.to_s
# => "Changed: Instance of SomeClass"
```

But in Clojuby, this will not change the `to_s` function:

```clojure
(rb/defclass SomeClass
  (to-s [{:keys [super]}] (str "Changed: " (super))))
(:to-s (rb/new SomeClass))
; => "Changed: #<SomeClass:0xe3b137c5>"

(def Stringify
  (rb/raw-eval
   "module Stringify
  def to_s
    \"Instance of #{self.class.to_s}\"
  end
end
Stringify"))

(rb/send SomeClass "include" Stringify)
(:to-s (rb/new SomeClass))
; => "Changed: #<SomeClass:0xe3b137c5>"
```

### Instantiating classes
Some classes map to Clojure object. So, if a Ruby class have a Clojure counterpart (like String, Hash, Set, Array, RegExp for example) they will be converted back to Clojure. This sometimes can interfer on how you interop with ruby classes that extend these parent classes. For example, if you create a class that inherits from Ruby's String:

```clojure
(rb/defclass SomeClass < rb/String
  (new-to-s [{:keys [self]}] (str "new " self)))

(:new-to-s (rb/new SomeClass))
; => nil
(rb/send (rb/new SomeClass) "new_to_s")
; => Crashes with NoMethodError
```

This happens because `rb/new` is trying to convert `SomeClass` to Clojure, and then back to Ruby. The "original class" info is lost on the conversion, and there's no way around this. To work around this issue, use `rb/raw-new` and/or `rb/raw-send`:

```clojure
(rb/send (rb/raw-new SomeClass (rb/clj->rb "Hello")) "new_to_s")
; => "new Hello"
```

Please notice that parameters _also need_ to be converted, otherwise this code will not work. This also is true for some Rails' magic, like `html_safe` - it adds some info on the new string that's lost on the conversion, so you _must use_ `raw-send` to convert:

```clojure
(rb/send (rb/send some-ruby-string "html_safe") "html_safe?")
; => false
(rb/send (rb/raw-send some-ruby-string "html_safe") "html_safe?")
; => true
```

## License

Copyright © 2017 Maurício Szabo

Distributed under the MIT License
