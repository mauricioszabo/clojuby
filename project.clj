(defproject link.szabo.mauricio/clojuby "0.0.1"
  :description "Calling Ruby from Clojure, with Clojure structures"
  :url "https://gitlab.com/mauricioszabo/clojuby"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.jruby/jruby "9.3.0.0"]]
  :profiles {:dev {:src-paths ["dev"]
                   :dependencies [[check "0.2.1-SNAPSHOT"]]}})
