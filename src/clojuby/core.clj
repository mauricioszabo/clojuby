(ns clojuby.core
  (:refer-clojure :exclude [eval send])
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as str]
            [clojure.walk :as walk])
  (:import [org.jruby Ruby RubyFixnum RubyHash RubyFloat RubyArray
            RubySymbol RubyString RubyBoolean RubyNil RubyObject RubyRegexp
            RubyClass RubyProc RubyModule RubyMethod RubyBasicObject
            RubyUnboundMethod]
           [org.jruby.ext.set RubySet]
           [org.jruby.javasupport JavaUtil]
           [org.jruby.runtime Block Block$Type Visibility Arity Signature
            CallBlock BlockCallback ThreadContext]
           [org.jruby.runtime.builtin IRubyObject]
           [org.jruby.internal.runtime.methods DynamicMethod CallConfiguration]
           [java.lang.reflect Method]))

#_(set! *warn-on-reflection* true)
(def ^:private ^Ruby runtime (Ruby/getGlobalRuntime))
(defn raw-eval [code]
  (.evalScriptlet runtime code))

(def ^:private ^RubyNil ruby-nil (raw-eval "nil"))
(def ^RubyClass ruby-set (raw-eval "require 'set'; Set"))
(def ^RubyClass ruby-class (raw-eval "Class"))
(def ^RubyClass ruby-object (raw-eval "Object"))
(def ^RubyModule ruby-module (raw-eval "Module"))
(def ^RubyObject ruby-main (raw-eval "self"))
(def ^:private ^RubyProc ruby-proc-wrapper
  (raw-eval "proc { |fn|
               proc { |*args, &b|
                 args << b if b
                 fn.invoke(self).invoke(*args)
               }
             }"))

(defn- arity-of-fn [f]
  (let [methods (-> f class .getDeclaredMethods)]
    (if (instance? clojure.lang.RestFn f)
      (-> ^clojure.lang.RestFn f .getRequiredArity inc -)
      (let [[method] (filter #(= "invoke" (.getName ^Method %)) methods)]
        (alength (.getParameterTypes ^Method method))))))

(defprotocol CljRubyObject (rb->clj [this]))
(defprotocol RubyCljObject (clj->rb ^IRubyObject [this]))

(defn- ^"[Lorg.jruby.runtime.builtin.IRubyObject;" normalize-args [args]
  (->> args (map clj->rb) (into-array IRubyObject)))

(defn- ^"[Lorg.jruby.runtime.builtin.IRubyObject;" as-args [args]
  (let [ruby-object? #(instance? IRubyObject %)]
    (assert (every? ruby-object? args)))
  (into-array IRubyObject args))

(defn- normalize-block [args]
  (let [possible-block (last args)]
    (if (instance? Block possible-block)
      [(vec (butlast args)) possible-block]
      [args Block/NULL_BLOCK])))

(defn raw-send [obj ^String method & args]
  (let [[args block] (normalize-block args)]
    (-> obj
        clj->rb
        (.callMethod (.getCurrentContext runtime)
                     method
                     (as-args args)
                     ^Block block))))

(defn send [obj ^String method & args]
  (let [[args block] (normalize-block args)]
    (-> obj
        clj->rb
        (.callMethod (.getCurrentContext runtime) method (normalize-args args) ^Block block)
        rb->clj)))

(defrecord Callback [function]
  BlockCallback
  (^IRubyObject call [this
                      ^ThreadContext context
                      ^"[Lorg.jruby.runtime.builtin.IRubyObject;" args
                      ^Block block-param]
    (->> args
         (map rb->clj)
         (apply function)
         clj->rb)))

(defn & [function]
  (CallBlock/newCallClosure ruby-main
                            ruby-object
                            (-> function arity-of-fn
                                Arity/createArity Signature/from)
                            (->Callback function)
                            (.getCurrentContext runtime)))

(def ^:private ^RubyProc special-proc
  (raw-eval "
proc { |&f|
  proc { |*args, &b| f.call(self, *args, &b) }
}"))

(defn && [function]
  (let [block (& function)
        new-block (.call special-proc (.getCurrentContext runtime) ^Block block)]
    (.getBlock ^RubyProc new-block)))

(defn proc [function]
  (let [block (& function)]
    (RubyProc/newProc runtime block Block$Type/PROC)))

(defn send& [obj ^String method & args]
  (let [block (-> args last &)
        args (butlast args)]
    (-> obj
        clj->rb
        (.callMethod (.getCurrentContext runtime) method (normalize-args args) ^Block block)
        rb->clj)))

(prefer-method print-method java.util.Map org.jruby.runtime.builtin.IRubyObject)
(prefer-method print-method java.util.RandomAccess org.jruby.runtime.builtin.IRubyObject)
(prefer-method print-method java.util.Set org.jruby.runtime.builtin.IRubyObject)
(prefer-method print-method java.util.List org.jruby.runtime.builtin.IRubyObject)

(defn- norm-fun-name [keyword]
  (-> keyword name (str/replace #"-" "_")))

(deftype WrappedRubyObject [^IRubyObject original-obj]
  clojure.lang.Associative
  (equiv [this x] (if (instance? WrappedRubyObject x)
                    (.equals original-obj (-> x meta :self))
                    (.equals original-obj x)))

  (assoc [this key val]
    (let [fun-name (norm-fun-name key)]
      (if (str/ends-with? fun-name "=")
        (do
          (send original-obj fun-name val)
          this)
        (let [dup (send original-obj "dup")]
          (send dup (str fun-name "=") val)
          dup))))

  (containsKey [_ key] (send original-obj "respond_to?" (norm-fun-name key)))

  clojure.lang.ILookup
  (valAt [this k] (send original-obj (-> k norm-fun-name (str/replace #"=$" ""))))
  (valAt [this k default]
    (let [res (get this k)]
      (if (nil? res) default res)))

  clojure.lang.IMeta
  (meta [_] {:self original-obj})

  Object
  (hashCode [_] (.hashCode original-obj))

  (equals [this x]
          (if (instance? WrappedRubyObject x)
            (.equals original-obj (-> x meta :self))
            (.equals original-obj x)))

  (toString [this] (send original-obj "inspect")))

(defn- norm-param [param]
  (cond
    (keyword? param) (keyword (norm-fun-name param))
    (string? param) (str/replace param #"-" "_")
    :else param))

(deftype WrappedRubyArrayLike [^IRubyObject original-obj]
  clojure.lang.Associative
  (equiv [this x] (if (instance? WrappedRubyArrayLike x)
                    (.equals original-obj (-> x meta :self))
                    (.equals original-obj x)))

  (assoc [this key val]
    (let [mutable-field (cond
                          (and (keyword? key) (-> key name (str/ends-with? "=")))
                          (-> key name (str/replace #"=$" "") keyword)

                          (and (string? key) (str/ends-with? key "="))
                          (str/replace key #"=$" ""))]
      (if mutable-field
        (do
          (send original-obj "[]=" key)
          this)
        (let [dup (send original-obj "dup")]
          (send dup "[]=" key val)
          dup))))

  ; (containsKey [_ key] (send original-obj "respond_to?" (norm-fun-name key)))

  clojure.lang.ILookup
  (valAt [this k] (send original-obj "[]" (norm-param k)))
  (valAt [this k default]
    (let [res (get this k)]
      (if (nil? res) default res)))

  clojure.lang.IMeta
  (meta [_] {:self original-obj})

  Object
  (hashCode [_] (.hashCode original-obj))

  (equals [this x]
          (if (instance? WrappedRubyArrayLike x)
            (.equals original-obj (-> x meta :self))
            (.equals original-obj x)))

  (toString [this] (send original-obj "inspect")))

(defn- ->Wrapped [obj]
  (if (send obj "respond_to?" "[]")
    (WrappedRubyArrayLike. obj)
    (WrappedRubyObject. obj)))

(extend-protocol CljRubyObject
  RubyFixnum
  (rb->clj [this] (.getLongValue this))

  RubySymbol
  (rb->clj [this] (-> this .asJavaString keyword))

  RubyString
  (rb->clj [this] (.decodeString this))

  RubyRegexp
  (rb->clj [this] (re-pattern (str (.to_s this))))

  RubyFloat
  (rb->clj [this] (.getDoubleValue this))

  RubyHash
  (rb->clj [this] (->> this
                       (map (fn [[k v]] [(rb->clj k) (rb->clj v)]))
                       (into {})))

  RubyArray
  (rb->clj [this] (mapv rb->clj this))

  RubyBoolean
  (rb->clj [this] (.isTrue this))

  RubyProc
  (rb->clj [this] (fn [ & args]
                    (let [[args block] (normalize-block args)]
                      (rb->clj (.call this
                                      (.getCurrentContext runtime)
                                      ^"[Lorg.jruby.runtime.builtin.IRubyObject;" (normalize-args args)
                                      ^Block block)))))

  Block
  (rb->clj [this] (fn [ & args]
                    (let [[args block] (normalize-block args)]
                      (rb->clj (.call this
                                      (.getCurrentContext runtime)
                                      ^"[Lorg.jruby.runtime.builtin.IRubyObject;" (normalize-args args)
                                      ^Block block)))))

  RubyMethod
  (rb->clj [this] (fn [ & args]
                    (let [[args block] (normalize-block args)]
                      (rb->clj (.call this
                                      (.getCurrentContext runtime)
                                      ^"[Lorg.jruby.runtime.builtin.IRubyObject;" (normalize-args args)
                                      ^Block block)))))

  RubyNil
  (rb->clj [_] nil)

  RubyObject
  (rb->clj [this] (case (-> this .getType .toString)
                    "Set" (->> (.callMethod this (.getCurrentContext runtime) "to_a")
                               (map rb->clj)
                               (into #{}))
                    (->Wrapped this)))

  IRubyObject
  (rb->clj [this] (->Wrapped this))

  Object
  (rb->clj [this] this)

  nil
  (rb->clj [this] nil))

(extend-protocol RubyCljObject
  java.lang.Long
  (clj->rb [this] (RubyFixnum. runtime this))

  java.lang.Double
  (clj->rb [this] (RubyFloat. runtime this))

  java.lang.String
  (clj->rb [this] (RubyString/newString runtime this))

  clojure.lang.Keyword
  (clj->rb [this] (.fastNewSymbol runtime (name this)))

  java.lang.Boolean
  (clj->rb [this] (RubyBoolean/newBoolean runtime this))

  java.util.regex.Pattern
  (clj->rb [this] (RubyRegexp/newRegexp runtime
                                        (str this)
                                        org.jruby.util.RegexpOptions/NULL_OPTIONS))

  nil
  (clj->rb [_] ruby-nil)

  java.util.Map
  (clj->rb [this]
    (if (instance? IRubyObject this)
      this
      (->> this
           (map (fn [[k v]] [(clj->rb k) (clj->rb v)]))
           (into {})
           (#(RubyHash/newHash runtime ^java.util.Map % ruby-nil)))))

  java.util.List
  (clj->rb [this]
    (if (instance? IRubyObject this)
      this
      (->> this
           (map clj->rb)
           (#(RubyArray/newArray runtime ^java.util.Collection %)))))

  java.util.Set
  (clj->rb [this]
    (if (instance? IRubyObject this)
      this
      (->> this
           (map clj->rb)
           (into-array IRubyObject)
           (RubySet/create (.getCurrentContext runtime) ruby-set))))

  clojure.lang.Fn
  (clj->rb [me]
    (cond
      (-> me meta :binding) (.getBlock
                             ^RubyProc
                             (.callMethod
                              ruby-proc-wrapper
                              (.getCurrentContext runtime)
                              "call"
                              (into IRubyObject [(JavaUtil/convertJavaToUsableRubyObject
                                                  runtime me)])
                              Block/NULL_BLOCK))

      (-> me meta :dont-convert?) me
      :else (proc me)))

  IRubyObject
  (clj->rb [this] this)

  WrappedRubyObject
  (clj->rb [this] (-> this meta :self))

  WrappedRubyArrayLike
  (clj->rb [this] (-> this meta :self))

  Object
  (clj->rb [this] (JavaUtil/convertJavaToUsableRubyObject runtime this)))

(defn eval [code]
  (-> code raw-eval rb->clj))

(defn rb-require [string]
  (let [norm (str/replace string #"\"" "\"\"")]
    (eval (str "require \"" norm "\""))))

(defn create-class!
  ([class-name parent] (create-class! class-name nil parent))
  ([class-name ruby-class-name parent]
   (assert (instance? RubyClass parent))
   (let [^RubyClass class (raw-send ruby-class "new" parent)]
     (.setBaseName class class-name)
     (when ruby-class-name
       (.defineConstant ruby-object
         (if (string? ruby-class-name)
           ruby-class-name
           class-name)
         class))
     class)))

(defn- method-gen [class visibility method-name call-fn arity]
  (fn method-gen []
    (proxy [DynamicMethod] [class
                            visibility
                            CallConfiguration/BACKTRACE_AND_SCOPE
                            method-name]
      (call ([^ThreadContext context,
              ^IRubyObject self,
              ^RubyModule class,
              ^String name,
              ^"[Lorg.jruby.runtime.builtin.IRubyObject;" args,
              ^Block block]
             (call-fn context self class name args block))
            ([^ThreadContext context,
              ^IRubyObject self,
              ^RubyModule class,
              ^String name,
              ^Block block]
             (call-fn context self class name [] block)))
      (dup [] (method-gen)))))

(defn- bindings-fn [^RubyClass class name]
  (let [super-sym (.fastNewSymbol runtime name)
        ancestors (rest (raw-send class "ancestors"))
        ^RubyUnboundMethod unbound (->> ancestors
                                        (map #(try
                                                (.instance_method ^RubyModule % super-sym)
                                                (catch Exception _ nil)))
                                        first)]
    (if unbound
      (fn [self]
        {:self self
         :super (fn [& args]
                  (let [bound (.bind unbound (.getCurrentContext runtime) self)
                        [args block] (normalize-block args)]
                    (.call bound
                           (.getCurrentContext runtime)
                           (normalize-args args)
                           ^Block block)))})
      (fn [self]
        {:self self
         :super (fn [ & _] (throw (ex-info (str "No superclass method for " name)
                                           {:method-name name
                                            :ancestor-list ancestors
                                            :current-class class})))}))))

(defn new-method [class method-name fun]
  (let [#_#_bindings (fn [self] {:self self
                                 :super (define-super-fn class self method-name)})
        bindings (bindings-fn class method-name)
        call-fn (fn [context self class name args block]
                  (if (= Block/NULL_BLOCK block)
                    (clj->rb (apply fun
                               (bindings self)
                               (map rb->clj args)))
                    (clj->rb (apply fun
                               (bindings self)
                               (map rb->clj (-> args vec (conj block)))))))
        arity (arity-of-fn fun)
        function-meta (meta fun)
        visibility (cond
                     (:private function-meta) Visibility/PRIVATE
                     (:protected function-meta) Visibility/PROTECTED
                     :else Visibility/PUBLIC)]
    ((method-gen class visibility method-name call-fn arity))))

(defn add-method! [^RubyClass ruby-class name function]
  (let [method-name (str/replace-first name "self." "")
        receiving-class (if (str/starts-with? name "self.")
                          (.getMetaClass ruby-class)
                          ruby-class)
        method (new-method ruby-class method-name function)]
    (.addMethod receiving-class method-name method)))

(spec/def ::ruby-class (spec/and qualified-symbol? #(-> % namespace (= "rb"))))
(spec/def ::rb-or-clj (spec/or :ruby ::ruby-class :clj symbol?))
(spec/def ::extends-part (spec/cat :arrow '#{<} :superclass ::rb-or-clj))
(spec/def ::includes-extends
  (spec/alt :include (spec/cat :_ '#{include} :module ::rb-or-clj)
            :extend (spec/cat :_ '#{extend} :module ::rb-or-clj)
            :prepend (spec/cat :_ '#{prepend} :module ::rb-or-clj)))

(spec/def ::method-definition (spec/cat :name simple-symbol?
                                        :arglist vector?
                                        :body (spec/* any?)))

(spec/def ::new-class-syntax
  (spec/cat :extends (spec/? ::extends-part)
            :body (spec/* (spec/or
                           :extends (spec/spec ::includes-extends)
                           :method (spec/spec ::method-definition)))))

(defn- get-ruby-class [[kind sym-name]]
  (if (= :ruby kind)
    `(raw-eval ~(-> sym-name name (str/replace #"\." "::")))
    sym-name))

(defmacro defclass [class-name & body]
  (let [comformed (spec/conform ::new-class-syntax body)
        {:keys [extends body]} comformed
        ruby-class (gensym "ruby-class-")]
    `(let [~ruby-class (create-class! ~(str class-name)
                                      ~(some-> class-name meta (:ruby-class (str class-name)))
                                      ~(get-ruby-class (:superclass
                                                        extends
                                                        [:ruby 'rb/Object])))]
       ~@(for [[command params] body]
           (case command
             :extends (let [[kind {:keys [module]}] params]
                        `(send ~ruby-class ~(name kind) ~(get-ruby-class module)))
             :method `(add-method! ~ruby-class
                                   ~(-> params :name str (str/replace #"-" "_"))
                                   (fn ~(:arglist params) ~@(:body params)))))
       (def ~class-name ~ruby-class))))

(defn set-variable [^RubyBasicObject self name value]
  (.setInstanceVariable self name (clj->rb value)))

(defn get-variable [^RubyBasicObject self name]
  (rb->clj (.getInstanceVariable self name)))

(defn raw-new [^RubyClass class & args]
  (let [[args block] (normalize-block args)]
    (.newInstance class
                  (.getCurrentContext runtime)
                  (as-args args)
                  ^Block block)))

(defn new [^RubyClass class & args]
  (let [[args block] (normalize-block args)
        arguments (->> args (map clj->rb) (into-array IRubyObject))]
    (rb->clj
     (.newInstance class
                   (.getCurrentContext runtime)
                   ^"[Lorg.jruby.runtime.builtin.IRubyObject;" arguments
                   ^Block block))))

(defprotocol SugarifiedSyntax
  (sugarify [_]))

(extend-protocol SugarifiedSyntax
  clojure.lang.Symbol
  (sugarify [this]
    (if (-> this namespace (= "rb"))
      `(eval ~(-> this name (str/replace #"\." "::")))
      this))

  clojure.lang.PersistentList
  (sugarify [this]
    (let [[fst & rst] this]
      (if (symbol? fst)
        (case fst
          . `(send ~(-> rst first sugarify)
                   ~(-> rst second name (str/replace #"-" "_"))
                   ~@(->> rst (drop  2) (map sugarify)))
          & `(clojuby.core/& ~@(map sugarify rst))
          && `(clojuby.core/&& ~@(map sugarify rst))
          (if (-> fst resolve meta :macro)
            this
            (map sugarify this))))))

  Object
  (sugarify [this] `(clj->rb ~this)))

(defmacro ruby [ & forms]
  (let [forms (map sugarify forms)]
    `(do ~@forms)))

(defmacro rb [obj]
  `(raw-eval ~(str/replace obj "." "::")))

(defmethod print-method IRubyObject [this ^java.io.Writer w]
  (.write w ^String (send this "inspect")))
(defmethod print-method WrappedRubyObject [this ^java.io.Writer w]
  (.write w ^String (:inspect this)))
(defmethod print-method WrappedRubyArrayLike [this ^java.io.Writer w]
  (.write w ^String (send this "inspect")))

; -- Helper macros for readers --
(defn as-ruby-obj [form]
  `(rb ~form))

(defn eval-ruby [form]
  `(ruby ~form))

(defn gem
  "Runs a gen command, exactly how you would run in the command line.

Example:

```
; To run gem install sequel:
(gem :install :sequel)
```"
  [ & args]
  (rb-require "rubygems/gem_runner.rb")
  (let [args (mapv #(cond-> % (keyword? %) name) args)
        runner (eval "Gem::GemRunner.new")]
    (send runner "run" args)))
